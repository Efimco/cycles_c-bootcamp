﻿using TMPro;
using UnityEngine;

public class Cycles_Task_4 : MonoBehaviour
{
    private string word = "";
    private int[] marks = new int[41];
    private string[] marksText = new string[3];

    void Start()
    {
        FillArray();
        DeclareMarks();

        for (int i = 0; i < marks.Length; i++)
        {
            word = CalculateWord(marks[i]);
            if (marks[i] <= 70)
            {
                Debug.Log("За " + marks[i] + word + "вы получили " + marksText[0]);
            }
            else if (marks[i] <= 80)
            {
                Debug.Log("За " + marks[i] + word + "вы получили " + marksText[1]);
            }
            else if (marks[i] <= 100)
            {
                Debug.Log("За " + marks[i] + word + "вы получили " + marksText[2]);
            }
        }
    }

    void DeclareMarks()
    {
        marksText[0] = "удовлетворительно";
        marksText[1] = "хорошо";
        marksText[2] = "отлично";
    }

    void FillArray()
    {
        marks[0] = 60;

        for (int i = 1; i < marks.Length; i++)
        {
            marks[i] = marks[i - 1] + 1;
        }
    }

    string CalculateWord(int number)
    {
        if (number % 10 == 1)
        {
            return " балл ";
        }
        else if (number % 10 < 5 && number % 10 != 0)
        {
            return " балла ";
        }
        else
        {
            return " баллов ";
        }
    }
}