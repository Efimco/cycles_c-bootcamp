﻿using System;
using TMPro;
using UnityEngine;

public class Cycles_Task_5 : MonoBehaviour
{
    public int number;
    private string numberText;
    private int fraction;


    private void Start()
    {
        fraction = number % 100;
        number = number - fraction;
        number = number / 100;
        int var1 = number % 10;
        number -= var1;
        number = number / 10;
        numberText = number.ToString() + fraction.ToString();
        number = Convert.ToInt32(numberText);
        Debug.Log(number);
    }
}