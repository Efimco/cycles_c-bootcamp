﻿using TMPro;
using UnityEngine;

public class Cycles_Task_2 : MonoBehaviour
{
    private TextMeshProUGUI text;
    private string chars = " ";

    void Start()
    {
        text = FindObjectOfType<TextMeshProUGUI>();
        if (text != null)
        {
            for (int i = 0; i < 5; i++)
            {
                chars += "0";
                text.text += chars + "\n";
            }
        }
    }
}