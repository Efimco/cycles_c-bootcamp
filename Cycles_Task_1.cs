﻿using System;
using UnityEngine;

public class Cycles_Task_1 : MonoBehaviour
{
    private int x = 1;
    private int sum = 0;

    private void Start()
    {
        Sum13();
    }

    private void Sum13()
    {
        while (x <= 13)
        {
            sum += x;
            x++;
        }

        Debug.Log(sum);
    }
}