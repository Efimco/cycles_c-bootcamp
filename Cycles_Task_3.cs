﻿using TMPro;
using UnityEngine;

public class Cycles_Task_3 : MonoBehaviour
{
    private TextMeshProUGUI text;
    private string chars = " ";
    private bool flag = false;

    void Start()
    {
        text = FindObjectOfType<TextMeshProUGUI>();
        if (text != null)
        {
            for (int i = 0; i < 5; i++)
            {
                if (!flag)
                {
                    chars = chars.Insert(0, "1");
                    text.text += chars + "\n";
                    flag = true;
                }
                else
                {
                    chars = chars.Insert(0, "0");
                    text.text += chars + "\n";
                    flag = false;
                }
            }
        }
    }
}